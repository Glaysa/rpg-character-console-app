﻿
using rpg_characters;
using rpg_characters.Characters;
using rpg_characters.Equipments;
using rpg_characters.Exceptions;
using System.Text;

PrimaryAttribute armourAttributes = new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 1,};
Weapon axe = new Weapon("Common axe", 1, WeaponTypes.AXES, EquipmentSlot.WEAPON, 7, 1.1);
Weapon hammer = new Weapon("Common sword", 10, WeaponTypes.HAMMERS, EquipmentSlot.WEAPON, 5, 1.1);
Armour plate = new Armour("Common plate", 1, ArmourTypes.PLATE, EquipmentSlot.BODY, armourAttributes);
Armour mail = new Armour("Common mail", 10, ArmourTypes.MAIL, EquipmentSlot.HEAD, armourAttributes);
Warrior warrior = new Warrior("Tank");

printHeader("Character Stats:");
warrior.PrintStat();

printHeader("Character Stats after equipping an axe weapon with a 7.7 DPS:");
warrior.EquipWeapon(axe);
warrior.PrintStat();

printHeader("Character Stats after leveling up to 2:");
warrior.LevelUp();
warrior.PrintStat();

printHeader("Character Stats after equipping an armour at level 2:");
warrior.EquipArmour(plate);
warrior.PrintStat();

printHeader("Character Stats after leveling up to 3:");
warrior.LevelUp();
warrior.PrintStat();

try
{
    printHeader("Equipping a weapon with a level requirement of 10:");
    warrior.EquipWeapon(hammer);
}
catch (InvalidWeaponException e)
{
    Console.WriteLine($"Warning: {e.Message} \nName: {hammer.Name} \nLevel Requirement: {hammer.LevelRequirement}");
}

try
{
    printHeader("Equipping an armour with a level requirement of 10:");
    warrior.EquipArmour(mail);
}
catch (InvalidArmourException e)
{
    Console.WriteLine($"Warning: {e.Message} \nName: {mail.Name} \nLevel Requirement: {mail.LevelRequirement}");
}

// Prints text to console with borders.

void printHeader(string header)
{
    StringBuilder sb = new StringBuilder();
    sb.Append("\n==================================================================\n");
    sb.Append(header);
    sb.Append("\n==================================================================\n");
    Console.WriteLine(sb.ToString());
}