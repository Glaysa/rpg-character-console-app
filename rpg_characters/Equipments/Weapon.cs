﻿
namespace rpg_characters.Equipments
{
    public class Weapon : Equipment
    {
        public WeaponTypes Type { get; set; }
        public double Damage { get; set; }
        public double AttackPerSecond { get; set; }

        public Weapon() { }

        public Weapon(string name, int levelRequirement, WeaponTypes type, EquipmentSlot slot, double damage, double attackPerSecond)
        {
            Name = name;
            LevelRequirement = levelRequirement;
            Type = type;
            Slot = slot;
            Damage = damage;
            AttackPerSecond = attackPerSecond;
        }
    }
}
