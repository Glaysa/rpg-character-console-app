﻿
namespace rpg_characters.Equipments
{
    public class Armour : Equipment
    {
        public ArmourTypes Type { get; set; }
        public PrimaryAttribute ArmourAttribute { get; set; } = new PrimaryAttribute();

        public Armour(){ }

        public Armour(string name, int levelRequirement, ArmourTypes type, EquipmentSlot slot, PrimaryAttribute armourAttriute)
        {
            Name = name;
            LevelRequirement = levelRequirement;
            Type = type;
            Slot = slot;
            ArmourAttribute = armourAttriute;
        }
    }
}
