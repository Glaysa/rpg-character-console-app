﻿
namespace rpg_characters.Equipments
{
    public class Equipment
    {
        public string Name { get; set; }
        public int LevelRequirement { get; set; }
        public EquipmentSlot Slot { get; set; }
    }
}
