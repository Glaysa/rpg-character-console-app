﻿
namespace rpg_characters
{
    public class PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public static PrimaryAttribute operator + (PrimaryAttribute p1, PrimaryAttribute p2)
        {
            return new PrimaryAttribute
            {
                Strength = p1.Strength + p2.Strength,
                Dexterity = p1.Dexterity + p2.Dexterity,
                Intelligence = p1.Intelligence + p2.Intelligence,
            };
        }
    }
}
