﻿
namespace rpg_characters.Exceptions
{
    public class InvalidArmourException : Exception
    {
        public override string Message => "Cannot equip armour.";
    }
}
