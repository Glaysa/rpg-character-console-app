﻿
namespace rpg_characters.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message => "Cannot equip weapon.";
    }
}
