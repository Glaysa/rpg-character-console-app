﻿
namespace rpg_characters
{
    public enum WeaponTypes
    {
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS,
    }

    public enum ArmourTypes
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    public enum EquipmentSlot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
