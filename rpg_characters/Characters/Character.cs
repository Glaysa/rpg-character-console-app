﻿using rpg_characters.Equipments;
using rpg_characters.Exceptions;
using System.Text;

namespace rpg_characters.Characters
{
    /// <summary>
    /// Upon instantiation, the character's default 'PrimaryAttribute' property is set and
    /// default 'DealDamage' property is calculated.
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }

        /// <summary>
        /// Contains the characters level and is by default 1 upon instantiation.
        /// </summary>
        public int Level { get; set; } = 1;

        /// <summary>
        /// Contains the total deal damage of a character.
        /// </summary>
        public double DealDamage { get; set; }

        /// <summary>
        /// This is the characters primary stats.
        /// </summary>
        public PrimaryAttribute PrimaryAttribute { get; set; } = new PrimaryAttribute();

        /// <summary>
        /// Contains the current weapon the character has equipped and its placement.
        /// </summary>
        public Dictionary<EquipmentSlot, Weapon> Weapon { get; set; } = new Dictionary<EquipmentSlot, Weapon>();

        /// <summary>
        /// Contains the current armour the character has equipped and its placement.
        /// </summary>
        public Dictionary<EquipmentSlot, Armour> Armour { get; set; } = new Dictionary<EquipmentSlot, Armour>();

        /// <summary>
        /// Checks whether the character is allowed to equip a given weapon
        /// based on the its type and level requirement.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>boolean</returns>
        protected abstract bool CanEquipWeapon(Weapon weapon);

        /// <summary>
        /// Checks whether the character is allowed to equip a given armour
        /// based on the its type and level requirement.
        /// </summary>
        /// <param name="armour"></param>
        /// <returns>boolean</returns>
        protected abstract bool CanEquipArmour(Armour armour);

        /// <summary>
        /// Level ups a character by 1 and increases all primary attributes and
        /// re-calculates the total deal damage by calling the CalculateDamage() method.
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Calculates a character's deal damage.
        /// </summary>
        /// <returns>double</returns>
        public abstract double CalculateDamage();

        /// <summary>
        /// Sets the character's weapon and re-calculates the total deal damage
        /// by calling the CalculateDamage() method.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>string: Success message </returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public virtual string EquipWeapon(Weapon weapon)
        {
            if (CanEquipWeapon(weapon))
            {
                Weapon[weapon.Slot] = weapon;
                DealDamage = CalculateDamage();
                return "New weapon equipped!";
            }
            else throw new InvalidWeaponException();
        }

        /// <summary>
        /// Sets the character's armour which increases the characters primary attributes 
        /// and re-calculates the total deal damage
        /// by calling the CalculateDamage() method.
        /// </summary>
        /// <param name="armour"></param>
        /// <returns>string: Success message </returns>
        /// <exception cref="InvalidArmourException"></exception>
        public virtual string EquipArmour(Armour armour)
        {
            if (CanEquipArmour(armour))
            {
                Armour[armour.Slot] = armour;
                PrimaryAttribute += armour.ArmourAttribute;
                DealDamage = CalculateDamage();
                return "New armour equipped!";
            }
            else throw new InvalidArmourException();
        }

        /// <summary>
        /// Prints the character's stats in the console.
        /// </summary>
        public virtual void PrintStat()
        {
            StringBuilder s = new StringBuilder();
            s.Append($"Name: {Name} \n");
            s.Append($"Level: {Level} \n");
            s.Append($"Strength: {PrimaryAttribute.Strength} \n");
            s.Append($"Dexterity: {PrimaryAttribute.Dexterity} \n");
            s.Append($"Intelligence: {PrimaryAttribute.Intelligence} \n");
            s.Append($"Damage: {DealDamage}");
            Console.WriteLine(s.ToString());
        }
    }
}
