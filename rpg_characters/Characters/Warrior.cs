﻿using rpg_characters.Equipments;

namespace rpg_characters.Characters
{
    public class Warrior : Character
    {
        public Warrior(string name)
        {
            Name = name;
            PrimaryAttribute.Strength = 5;
            PrimaryAttribute.Dexterity = 2;
            PrimaryAttribute.Intelligence = 1;
            DealDamage = CalculateDamage();
        }

        public override void LevelUp()
        {
            Level++;
            PrimaryAttribute.Strength += 3;
            PrimaryAttribute.Dexterity += 2;
            PrimaryAttribute.Intelligence += 1;
            DealDamage = CalculateDamage();
        }

        public override double CalculateDamage()
        {
            if(Weapon.Count >= 1) {
                Weapon weapon = Weapon.Values.First();
                double weaponDPS = weapon.Damage * weapon.AttackPerSecond;
                double damage = weaponDPS * (1 + ((double) PrimaryAttribute.Strength / 100));
                return Math.Round(damage, 2);
            }
            return Math.Round(1 * (1 + ((double) PrimaryAttribute.Strength / 100)), 2);
        }

        protected override bool CanEquipArmour(Armour armour)
        {
            bool canEquipArmourType = armour.Type == ArmourTypes.MAIL || armour.Type == ArmourTypes.PLATE;
            bool canEquipArmourLevel = Level >= armour.LevelRequirement;
            return canEquipArmourType && canEquipArmourLevel;
        }

        protected override bool CanEquipWeapon(Weapon weapon)
        {
            bool canEquipWeaponType = weapon.Type == WeaponTypes.AXES || weapon.Type == WeaponTypes.HAMMERS || weapon.Type == WeaponTypes.SWORDS;
            bool canEquipWeaponLevel = Level >= weapon.LevelRequirement;
            return canEquipWeaponType && canEquipWeaponLevel;
        }
    }
}
