﻿using rpg_characters.Equipments;

namespace rpg_characters.Characters
{
    public class Mage : Character
    {
        public Mage(string name)
        {
            Name = name;
            PrimaryAttribute.Strength = 1;
            PrimaryAttribute.Dexterity = 1;
            PrimaryAttribute.Intelligence = 8;
            DealDamage = CalculateDamage();
        }

        public override void LevelUp()
        {
            Level++;
            PrimaryAttribute.Strength += 1;
            PrimaryAttribute.Dexterity += 1;
            PrimaryAttribute.Intelligence += 5;
            DealDamage = CalculateDamage();
        }
        public override double CalculateDamage()
        {
            if (Weapon.Count >= 1)
            {
                Weapon weapon = Weapon.Values.First();
                double weaponDPS = weapon.Damage * weapon.AttackPerSecond;
                double damage = weaponDPS * (1 + ((double) PrimaryAttribute.Intelligence / 100));
                return Math.Round(damage, 2);
            }
            return Math.Round(1 * (1 + ((double)PrimaryAttribute.Intelligence / 100)), 2);
        }

        protected override bool CanEquipArmour(Armour armour)
        {
            bool canEquipArmourType = armour.Type == ArmourTypes.CLOTH;
            bool canEquipArmourLevel = Level >= armour.LevelRequirement;
            return canEquipArmourType && canEquipArmourLevel;
        }

        protected override bool CanEquipWeapon(Weapon weapon)
        {
            bool canEquipWeaponType = weapon.Type == WeaponTypes.STAFFS || weapon.Type == WeaponTypes.WANDS;
            bool canEquipWeaponLevel = Level >= weapon.LevelRequirement;
            return canEquipWeaponType && canEquipWeaponLevel;
        }
    }
}
