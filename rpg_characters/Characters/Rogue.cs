﻿using rpg_characters.Equipments;

namespace rpg_characters.Characters
{
    public class Rogue : Character
    {
        public Rogue(string name)
        {
            Name = name;
            PrimaryAttribute.Strength = 2;
            PrimaryAttribute.Dexterity = 6;
            PrimaryAttribute.Intelligence = 1;
            DealDamage = CalculateDamage();
        }

        public override void LevelUp()
        {
            Level++;
            PrimaryAttribute.Strength += 1;
            PrimaryAttribute.Dexterity += 4;
            PrimaryAttribute.Intelligence += 1;
            DealDamage = CalculateDamage();
        }

        public override double CalculateDamage()
        {
            if (Weapon.Count >= 1)
            {
                Weapon weapon = Weapon.Values.First();
                double weaponDPS = weapon.Damage * weapon.AttackPerSecond;
                double damage = weaponDPS * (1 + ((double) PrimaryAttribute.Dexterity / 100));
                return Math.Round(damage, 2);
            }
            return Math.Round(1 * (1 + ((double)PrimaryAttribute.Dexterity / 100)), 2);
        }

        protected override bool CanEquipArmour(Armour armour)
        {
            bool canEquipArmourType = armour.Type == ArmourTypes.LEATHER || armour.Type == ArmourTypes.MAIL;
            bool canEquipArmourLevel = Level >= armour.LevelRequirement;
            return canEquipArmourType && canEquipArmourLevel;
        }

        protected override bool CanEquipWeapon(Weapon weapon)
        {
            bool canEquipWeaponType = weapon.Type == WeaponTypes.DAGGERS || weapon.Type == WeaponTypes.SWORDS;
            bool canEquipWeaponLevel = Level >= weapon.LevelRequirement;
            return canEquipWeaponType && canEquipWeaponLevel;
        }
    }
}
