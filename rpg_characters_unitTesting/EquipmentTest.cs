﻿using rpg_characters;
using rpg_characters.Characters;
using rpg_characters.Equipments;
using rpg_characters.Exceptions;

namespace rpg_characters_unitTesting
{
    public class EquipmentTest
    {
        [Fact]
        public void EquipWeapon_LevelTooHigh_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 2,
                Slot = EquipmentSlot.WEAPON,
                Type = WeaponTypes.AXES,
                Damage = 7,
                AttackPerSecond = 1.1
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmour_LevelTooHigh_ShouldThrowInvalidArmourException()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Armour testPlateBody = new Armour()
            {
                Name = "Common plate body armor",
                LevelRequirement = 2,
                Slot = EquipmentSlot.BODY,
                Type = ArmourTypes.PLATE,
                ArmourAttribute = new PrimaryAttribute() { Strength = 1 }
            };

            // Act & Assert
            Assert.Throws<InvalidArmourException>(() => warrior.EquipArmour(testPlateBody));
        }

        [Fact]
        public void EquipWeapon_WrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Weapon testBow = new Weapon()
            {
                Name = "Common bow",
                LevelRequirement = 1,
                Slot = EquipmentSlot.WEAPON,
                Type = WeaponTypes.BOWS,
                Damage = 12,
                AttackPerSecond = 0.8
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipArmour_WrongArmourType_ShouldThrowInvalidArmourException()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Armour testClothBody = new Armour()
            {
                Name = "Common cloth body armor",
                LevelRequirement = 1,
                Slot = EquipmentSlot.BODY,
                Type = ArmourTypes.CLOTH,
                ArmourAttribute = new PrimaryAttribute() { Intelligence = 5 }
            };

            // Act && Assert
            Assert.Throws<InvalidArmourException>(() => warrior.EquipArmour(testClothBody));
        }

        [Fact]
        public void EquipWeapon_ValidWeaponType_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                Slot = EquipmentSlot.WEAPON,
                Type = WeaponTypes.AXES,
                Damage = 7,
                AttackPerSecond = 1.1
            };

            // Act
            string actual = warrior.EquipWeapon(testAxe);
            string expected = "New weapon equipped!";

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmour_ValidArmourType_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Armour testPlateBody = new Armour()
            {
                Name = "Common plate body armor",
                LevelRequirement = 1,
                Slot = EquipmentSlot.BODY,
                Type = ArmourTypes.PLATE,
                ArmourAttribute = new PrimaryAttribute() { Strength = 1 }
            };

            // Act
            string actual = warrior.EquipArmour(testPlateBody);
            string expected = "New armour equipped!";

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_NoWeaponIsEquipped_ShouldReturnDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");

            // Act
            double actual = warrior.CalculateDamage();
            double expected = Math.Round(1 * (1 + (5.0 / 100)), 2);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WeaponIsEquipped_ShouldReturnDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                Slot = EquipmentSlot.WEAPON,
                Type = WeaponTypes.AXES,
                Damage = 7,
                AttackPerSecond = 1.1
            };

            // Act
            warrior.EquipWeapon(testAxe);
            double actual = warrior.CalculateDamage();
            double expected = Math.Round((7 * 1.1) * (1 + ((double) 5 / 100)), 2);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamaage_WeaponAndArmourEquipped_ShouldReturnDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                Slot = EquipmentSlot.WEAPON,
                Type = WeaponTypes.AXES,
                Damage = 7,
                AttackPerSecond = 1.1
            };

            Armour testPlateBody = new Armour()
            {
                Name = "Common plate body armor",
                LevelRequirement = 1,
                Slot = EquipmentSlot.BODY,
                Type = ArmourTypes.PLATE,
                ArmourAttribute = new PrimaryAttribute() { Strength = 1 }
            };

            // Act
            warrior.EquipArmour(testPlateBody);
            warrior.EquipWeapon(testAxe);
            double actual = warrior.CalculateDamage();
            double expected = Math.Round((7 * 1.1) * (1 + ((double) (5 + 1) / 100)), 2);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
