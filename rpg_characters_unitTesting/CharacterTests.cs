using rpg_characters;
using rpg_characters.Characters;

namespace rpg_characters_unitTesting
{
    public class CharacterTests
    {
        [Fact]
        public void CreateCharacter_InitializeLevel_ShouldBeDefaultLevelOne()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            int actual = warrior.Level;
            int expected = 1;

            // Act & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_IncreaseLevelByOne_ShouldBeLevelTwo()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");

            // Act
            warrior.LevelUp();
            int actual = warrior.Level;
            int expected = 2;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateCharacter_InitializeMagePrimaryAttributes_ShouldBeMageDefaultAttributes()
        {
            // Arrange
            Mage mage = new Mage("Yelan");
            PrimaryAttribute actual = mage.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };

            // Act & Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void CreateCharacter_InitializeRangerPrimaryAttributes_ShouldBeRangerDefaultAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Robin");
            PrimaryAttribute actual = ranger.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
            };

            // Act & Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void CreateCharacter_InitializeRoguePrimaryAttributes_ShouldBeRogueDefaultAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue("David");
            PrimaryAttribute actual = rogue.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
            };

            // Act & Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void CreateCharacter_InitializeWarriorPrimaryAttributes_ShouldBeWarriorDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");
            PrimaryAttribute actual = warrior.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
            };

            // Act & Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void LevelUp_IncreaseMagePrimaryAttributes_ShouldIncreaseMageDefaultAttributes()
        {
            // Arrange
            Mage mage = new Mage("Yelan");

            // Act
            mage.LevelUp();
            PrimaryAttribute actual = mage.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
            };

            // Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void LevelUp_IncreaseRangerPrimaryAttributes_ShouldIncreaseRangerDefaultAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Robin");

            // Act
            ranger.LevelUp();
            PrimaryAttribute actual = ranger.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
            };

            // Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void LevelUp_IncreaseRoguePrimaryAttributes_ShouldIncreaseRogueDefaultAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue("David");

            // Act
            rogue.LevelUp();
            PrimaryAttribute actual = rogue.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2,
            };

            // Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }

        [Fact]
        public void LevelUp_IncreaseWarriorPrimaryAttributes_ShouldIncreaseWarriorDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Tank");

            // Act
            warrior.LevelUp();
            PrimaryAttribute actual = warrior.PrimaryAttribute;
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
            };

            // Assert
            Assert.True(
                actual.Strength == expected.Strength &&
                actual.Dexterity == expected.Dexterity &&
                actual.Intelligence == expected.Intelligence
                );
        }
    }
}