
# RPG Characters Console App

This task is provided by **Noroff** and was implemented by **Glaysa Fernandez**.

### Task Description
Create a console app with the following requirements:
- The app must accomodate 4 different types of character.
- The characters' stats vary based on their primary attributes and equipments.
- The character must be able to equip an armour and a weapon that affects the overall attributes of the character.

### Game

There is no game implementation. To run the app, clone this repo over HTTPS and either run
it using the test folder or exceute the main method.

### Game Details

The characters can only equip weapons and armours if it is the correct type and the character level meets the level requirement of the equipment.

**Characters**
- Mage
- Ranger
- Rogue
- Warrior

**Weapons:** Increases character damage
- Axe
- Bow
- Dagger
- Hammer
- Staff
- Sword
- Wand

**Armours:** Increases character attributes
- Cloth
- Mail
- Plate
- Leather